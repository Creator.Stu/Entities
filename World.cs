﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Creator.Entities
{
    public class World : IDisposable
    {
        #region Static
        public const string Default = "Default";

        private static readonly Dictionary<string, World> m_AllWorlds = new Dictionary<string, World>() { { Default, new World(Default) } };

        public static World Active { get; set; }

        public static World CreateWorld(string name) {
            World world = GetWorld(name);
            if (world == null) {
                world = new World(name);
                m_AllWorlds.Add(name, world);
            }
            return world;
        }

        public static World GetWorld(string name) {
            if (m_AllWorlds.ContainsKey(name)) {
                return m_AllWorlds[name];
            }
            return null;
        }

        public static void RemoveWorld(string name) {
            World world = GetWorld(name);
            if (world != null) {
                world.Dispose();
                m_AllWorlds.Remove(name);
            }
        }

        public static void DisposeAllWorlds() {
            foreach (World world in m_AllWorlds.Values) {
                world.Dispose();
            }

            m_AllWorlds.Clear();
        }

        private class SystemProxy : IComparable<SystemProxy>
        {
            public ISystem system;
            public int priority;

            public SystemProxy(ISystem s, int prior) { system = s; priority = prior; }

            public int CompareTo(SystemProxy other) { return other.priority - priority; } // 按降序排序
        }

        private static void CollectSystems(World world) {
            Type sysType = typeof(ISystem);

            IEnumerable<Type> types = AppDomain.CurrentDomain.GetAssemblies()
                                        .SelectMany(s => s.GetTypes())
                                        .Where(t => t.IsClass && t.IsPublic && !t.IsAbstract
                                                && sysType.IsAssignableFrom(t));

            List<SystemProxy> proxys = new List<SystemProxy>();
            string worldName = world.name;

            Type worldType = typeof(WorldAttribute);
            foreach (Type t in types) {
                object[] attributes = t.GetCustomAttributes(worldType, false);
                for (int i = 0; i < attributes.Length; i++) {
                    WorldAttribute attribute = (WorldAttribute)attributes[i];
                    if (attribute.name != worldName) {
                        continue;
                    }

                    ISystem system = (ISystem)Activator.CreateInstance(t);
                    proxys.Add(new SystemProxy(system, attribute.priority));
                    break;
                }
            }

            proxys.Sort();

            world.m_Systems.AddRange(proxys.Select(proxy => { return proxy.system; }));
        }
        #endregion Static

        #region Non-Static
        private bool isCreated = true;

        public string name { get; private set; }

        private World(string name) {
            this.name = name;
            CollectSystems(this);
        }

        #region System Operations
        private readonly Dictionary<Type, ISystem> m_SystemLookup = new Dictionary<Type, ISystem>();

        private readonly List<ISystem> m_Systems = new List<ISystem>();

        public ReadOnlyCollection<ISystem> Systems => new ReadOnlyCollection<ISystem>(m_Systems);
        #endregion System Operations

        #region Entity Operations
        private readonly List<IEntity> m_Entities = new List<IEntity>();

        public T CreateEntity<T>() where T : class, IEntity {
            return (T)CreateEntity(typeof(T));
        }

        public IEntity CreateEntity(Type t) {
            IEntity entity = EntityPool.Spawn(t);
            entity.Retain(this);
            m_Entities.Add(entity);

            entity.OnComponentAdded += OnEntityUpdated;
            entity.OnComponentRemoved += OnEntityUpdated;

            foreach (IGroup group in m_Groups.Values) {
                group.HandleEntity(entity);
            }
            return entity;
        }

        public IEntity CloneEntity(IEntity entity) {
            IEntity _new = entity.Clone();
            entity.Retain(this);
            m_Entities.Add(_new);
            _new.OnComponentAdded += OnEntityUpdated;
            _new.OnComponentRemoved += OnEntityUpdated;
            foreach (IGroup group in m_Groups.Values)
            {
                group.HandleEntity(entity);
            }
            return _new;
        }

        public void DestroyEntity(IEntity entity) {
            m_Entities.Remove(entity);
            entity.OnComponentAdded -= OnEntityUpdated;
            entity.OnComponentRemoved -= OnEntityUpdated;
            entity.Release(this);
            foreach (IGroup group in m_Groups.Values)
            {
                group.HandleEntity(entity);
            }
        }

        public void DestroyAllEntities() {
            for (int i = m_Entities.Count - 1; i >= 0; i--) {
                DestroyEntity(m_Entities[i]);
            }
        }

        public IEntity[] GetEntities() {
            return m_Entities.ToArray();
        }

        public TEntity[] GetEntities<TEntity>() where TEntity : class, IEntity {
            return m_Entities.Where(e => { return e is TEntity; }).Select(e => { return e as TEntity; }).ToArray();
        }

        public IEntity[] GetEntities(IMatcher<IEntity> matcher) {
            return m_Entities.Where(e => { return matcher.IsMatch(e); }).ToArray();
        }

        public bool HasEntity(IEntity entity) {
            return m_Entities.Contains(entity);
        }

        private void OnEntityUpdated(IEntity entity, Component component) {
            foreach (IGroup group in m_Groups.Values)
            {
                group.HandleEntity(entity);
            }
        }


        #endregion Entity Operations

        #region Group Operations
        readonly Dictionary<IMatcher, IGroup> m_Groups = new Dictionary<IMatcher, IGroup>();

        public IGroup GetGroup(IMatcher matcher) {
            IGroup group;
            if (!m_Groups.TryGetValue(matcher, out group)) {
                group = new Group(matcher);
                for (int i = 0; i < m_Entities.Count; i++) {
                    group.HandleEntity(m_Entities[i]);
                }
            }
            return group;
        }
        #endregion Group Operations

        public void Dispose()
        {
            if (!isCreated) return;
            isCreated = false;

            DestroyAllEntities();
            m_SystemLookup.Clear();
            m_Systems.Clear();

            m_Groups.Clear();
        }

        #endregion Non-Static
    }

    /// <summary>
    /// 用于标记System在哪个World会被创建，默认为Default
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class WorldAttribute : Attribute {
        public readonly string name;
        public readonly int priority;

        public WorldAttribute(string name, int priority = 0) { this.name = name; this.priority = priority; }
    }
}
