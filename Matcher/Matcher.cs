﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Creator.Entities
{
    public class Matcher<TEntity> : IAllOfMatcher<TEntity>, IAnyOfMatcher<TEntity>, INoneOfMatcher<TEntity> where TEntity : class, IEntity
    {
        private List<Type> m_All = new List<Type>();

        private List<Type> m_Any = new List<Type>();

        private List<Type> m_None = new List<Type>();

        public ReadOnlyCollection<Type> All => m_All.AsReadOnly();

        public ReadOnlyCollection<Type> Any => m_Any.AsReadOnly();

        public ReadOnlyCollection<Type> None => m_None.AsReadOnly();

        #region Constructor
        public Matcher() { }

        public Matcher(params Matcher<TEntity>[] matchers)
        {
            AllOf(matchers);
            AnyOf(matchers);
            NoneOf(matchers);
        }
        # endregion Constructor

        #region IMatcher
        public bool IsMatch(IEntity entity)
        {
            return IsMatch(entity as TEntity);
        }

        public bool IsMatch(TEntity entity)
        {
            return entity != null
                && (All.Count == 0 || entity.HasAllComponents(All.ToArray()))
                && (Any.Count == 0 || entity.HasAnyComponent(Any.ToArray()))
                && (None.Count == 0 || !entity.HasAnyComponent(None.ToArray()));
        }
        #endregion IMatcher

        #region IAllOfMatcher
        public IAllOfMatcher<TEntity> AllOf(params Type[] types)
        {
            m_All = m_All.Union(types).ToList();
            return this;
        }

        public IAllOfMatcher<TEntity> AllOf(params IAllOfMatcher<TEntity>[] matchers)
        {
            IEnumerable<Type> all = m_All.AsEnumerable();
            foreach (IAllOfMatcher<TEntity> matcher in matchers)
            {
                all = all.Union(matcher.All);
            }
            m_All = all.ToList();
            return this;
        }

        #endregion IAllOfMatcher

        #region IAnyOfMatcher
        public IAnyOfMatcher<TEntity> AnyOf(params Type[] types)
        {
            m_Any = m_Any.Union(types).ToList();
            return this;
        }

        public IAnyOfMatcher<TEntity> AnyOf(params IAnyOfMatcher<TEntity>[] matchers)
        {
            IEnumerable<Type> any = m_Any.AsEnumerable();
            foreach (IAnyOfMatcher<TEntity> matcher in matchers)
            {
                any = any.Union(matcher.Any);
            }
            m_Any = any.ToList();
            return this;
        }
        #endregion IAnyOfMatcher

        #region INoneOfMatcher
        public INoneOfMatcher<TEntity> NoneOf(params Type[] types)
        {
            m_None = m_None.Union(types).ToList();
            return this;
        }

        public INoneOfMatcher<TEntity> NoneOf(params INoneOfMatcher<TEntity>[] matchers)
        {
            IEnumerable<Type> none = m_None.AsEnumerable();
            foreach (INoneOfMatcher<TEntity> matcher in matchers)
            {
                none = none.Union(matcher.None);
            }
            m_None = none.ToList();
            return this;
        }
        #endregion INoneOfMatcher

        public override string ToString()
        {
            const string separator = ", ";
            int lastSeparator = 0;
            StringBuilder sb = new StringBuilder();
            sb.Append($"{this.GetType().Name}:\n");

            sb.Append("All: { ");
            lastSeparator = m_All.Count - 1;
            for (int i = 0; i < m_All.Count; i++)
            {
                sb.Append($"{m_All[i].Name}{(i < lastSeparator ? separator : "")}");
            }
            sb.Append(" }\n");

            sb.Append("Any: { ");
            lastSeparator = m_Any.Count - 1;
            for (int i = 0; i < m_Any.Count; i++)
            {
                sb.Append($"{m_Any[i].Name}{(i < lastSeparator ? separator : "")}");
            }
            sb.Append(" }\n");

            sb.Append("None: { ");
            lastSeparator = m_None.Count - 1;
            for (int i = 0; i < m_None.Count; i++)
            {
                sb.Append($"{m_None[i].Name}{(i < lastSeparator ? separator : "")}");
            }
            sb.Append(" }\n");

            return sb.ToString();
        }
    }

}
