﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Creator.Entities
{

    public interface IAnyOfMatcher<TEntity> : IMatcher<TEntity> where TEntity : class, IEntity
    {
        ReadOnlyCollection<Type> Any { get; }

        IAnyOfMatcher<TEntity> AnyOf(params Type[] types);

        IAnyOfMatcher<TEntity> AnyOf(params IAnyOfMatcher<TEntity>[] matchers);
    }
}
