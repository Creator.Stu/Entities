﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Creator.Entities
{
    public interface IMatcher
    {
        bool IsMatch(IEntity entity);
    }

    public interface IMatcher<TEntity> : IMatcher where TEntity : class, IEntity
    {
        bool IsMatch(TEntity entity);
    }
}
