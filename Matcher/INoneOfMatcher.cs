﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Creator.Entities
{
    public interface INoneOfMatcher<TEntity> : IMatcher<TEntity> where TEntity : class, IEntity
    {
        ReadOnlyCollection<Type> None { get; }

        INoneOfMatcher<TEntity> NoneOf(params Type[] types);

        INoneOfMatcher<TEntity> NoneOf(params INoneOfMatcher<TEntity>[] matchers);
    }
}
