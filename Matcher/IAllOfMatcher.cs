﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Creator.Entities
{
    public interface IAllOfMatcher<TEntity> : IMatcher<TEntity> where TEntity : class, IEntity
    {
        ReadOnlyCollection<Type> All { get; }

        IAllOfMatcher<TEntity> AllOf(params Type[] types);

        IAllOfMatcher<TEntity> AllOf(params IAllOfMatcher<TEntity>[] matchers);
    }
}
