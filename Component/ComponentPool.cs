﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Creator.Entities
{
    public static class ComponentPool
    {
        public readonly static Type IComponentType = typeof(Component);

        private readonly static Dictionary<Type, Stack<Component>> m_Pool = new Dictionary<Type, Stack<Component>>();


        static ComponentPool() { }

        public static T Spawn<T>() where T : Component
        {
            return (T)Spawn(typeof(T));
        }

        public static Component Spawn(Type t)
        {
            if (!IComponentType.IsAssignableFrom(t)) {
                throw new InvalidCastException($"{t.FullName} is not a subclass of {IComponentType.FullName} !");
            }

            Component component = null;
            if (m_Pool.ContainsKey(t) && m_Pool[t].Count > 0)
            {
                component = m_Pool[t].Pop();
            }
            else {
                component = (Component)Activator.CreateInstance(t);
            }
            component.OnRetain();
            return component;
        }

        public static void Despawn(Component component) {
            if (component == null) {
                throw new ArgumentNullException(nameof(component));
            }

            Type t = component.GetType();
            if (!m_Pool.ContainsKey(t))
            {
                m_Pool.Add(t, new Stack<Component>());
            }
            m_Pool[t].Push(component);
        }
    }
}
