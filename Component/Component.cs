﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Creator.Entities
{
    public abstract class Component
    {
        protected virtual void OnCreate() { }

        protected virtual void OnDestroy() { }

        internal void OnRetain() {
            OnCreate();
        }
        internal void OnRelease() {
            OnDestroy();
        }
    }
}
