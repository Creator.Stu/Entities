﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Creator.Entities
{
    public class Group : IGroup
    {
        private readonly HashSet<IEntity> m_Entities = new HashSet<IEntity>(EntityEqualityComparer<IEntity>.comparer);

        public IMatcher matcher { get; private set; }

        internal Group(IMatcher matcher) {
            this.matcher = matcher;
        }

        public void HandleEntity(IEntity entity) {
            if (matcher.IsMatch(entity))
            {
                AddEntity(entity);
            }
            else
            {
                RemoveEntity(entity);
            }
        }

        private bool AddEntity(IEntity entity) {
            if (entity.isActive) {
                bool added = m_Entities.Add(entity);
                if (added)
                {
                    entity.Retain(this);
                }
                return added;
            }
            return false;
        }

        private bool RemoveEntity(IEntity entity) {
            bool removed = m_Entities.Remove(entity);
            if (removed) {
                entity.Release(this);
            }
            return removed;
        }

        public bool Contains(IEntity entity) {
            return m_Entities.Contains(entity);
        }

        public IEntity[] GetEntities() {
            return m_Entities.ToArray();
        }

        public IEnumerable<IEntity> AsEnumerable() {
            return m_Entities;
        }

        public HashSet<IEntity>.Enumerator GetEnumerator() {
            return m_Entities.GetEnumerator();
        }

        public override string ToString()
        {
            return $"Group({matcher})";
        }
    }
}
