﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Creator.Entities
{
    public interface IGroup
    {
        IMatcher matcher { get; }

        void HandleEntity(IEntity entity);

        bool Contains(IEntity entity);

        IEntity[] GetEntities();

        IEnumerable<IEntity> AsEnumerable();

        HashSet<IEntity>.Enumerator GetEnumerator();
    }
}
