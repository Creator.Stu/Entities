﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Creator.Entities
{
    public delegate void ComponentChanged(IEntity entity, Component component);
    public delegate void EntityEvent(IEntity entity);

    public interface IEntity : IAERC
    {
        event ComponentChanged OnComponentAdded;
        event ComponentChanged OnComponentRemoved;
        event EntityEvent OnEntityReleased;

        bool isActive { get; set; }

        IAERC aerc { get; }

        void Initialize(int id);

        int GetInstanceID();

        T AddComponent<T>() where T : class, Component;

        Component AddComponent(Type t);

        T GetComponent<T>() where T : class, Component;

        Component GetComponent(Type t);

        bool HasComponent<T>() where T : class, Component;

        bool HasComponent(Type t);

        bool HasAnyComponent(params Type[] types);

        bool HasAllComponents(params Type[] types);

        void RemoveComponent<T>() where T : class, Component;

        void RemoveComponent(Type t);

        void RemoveAllComponents();

        IEntity Clone();
    }
}
