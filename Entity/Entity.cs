﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Creator.Entities
{
    public class Entity : IEntity
    {
        public event ComponentChanged OnComponentAdded;
        public event ComponentChanged OnComponentRemoved;
        public event EntityEvent OnEntityReleased;

        private int m_ID;

        private IAERC _aerc;

        private readonly Dictionary<Type, Component> m_Components = new Dictionary<Type, Component>();

        public bool isActive { get; set; } = true;

        public IAERC aerc { get { return _aerc; } }

        public int retainCount { get { return aerc.retainCount; } } 

        public void Initialize(int id) {
            m_ID = id;
            _aerc = aerc ?? new SafeAERC(this);
            m_Components.Clear();
        }

        public int GetInstanceID() {
            return m_ID;
        }

        public T AddComponent<T>() where T : class, Component {
            return (T)AddComponent(typeof(T));
        }

        public Component AddComponent(Type t) {
            if (HasComponent(t)) {
                throw new ArgumentException($"An item with the same key has already been added. Key: {t.FullName}");
            }
            Component component = ComponentPool.Spawn(t);
            m_Components.Add(t, component);

            OnComponentAdded?.Invoke(this, component);

            return component;
        }

        public T GetComponent<T>() where T : class, Component {
            return (T)GetComponent(typeof(T));
        }

        public Component GetComponent(Type t) {
            if (!HasComponent(t)) {
                throw new KeyNotFoundException($"The given key \"{t.FullName}\" was not present in the dictionary. ");
            }

            return m_Components[t];
        }

        public bool HasComponent<T>() where T : class, Component {
            return HasComponent(typeof(T));
        }

        public bool HasComponent(Type t) {
            return m_Components.ContainsKey(t);
        }

        public bool HasAnyComponent(params Type[] types) {
            return types.Any(t => m_Components.ContainsKey(t));
        }

        public bool HasAllComponents(params Type[] types) {
            return types.All(t => m_Components.ContainsKey(t));
        }

        public void RemoveComponent<T>() where T : class, Component {
            RemoveComponent(typeof(T));
        }

        public void RemoveComponent(Type t) {
            if (!HasComponent(t))
            {
                throw new KeyNotFoundException($"The given key \"{t.FullName}\" was not present in the dictionary. ");
            }

            Component component = m_Components[t];
            m_Components.Remove(t);
            OnComponentRemoved?.Invoke(this, component);
            ComponentPool.Despawn(component);
        }

        public void RemoveAllComponents() {
            var types = m_Components.Keys.ToArray();
            foreach (Type type in types) {
                RemoveComponent(type);
            }
        }

        public void Retain(object owner) {
            aerc.Retain(owner);
        }

        public void Release(object owner) {
            aerc.Release(owner);

            if (retainCount == 0) {
                isActive = false;
                RemoveAllComponents();
                OnEntityReleased?.Invoke(this);
                OnComponentAdded = null;
                OnComponentRemoved = null;
                OnEntityReleased = null;
                EntityPool.Despawn(this);
            }
        }

        public IEntity Clone()
        {
            IEntity entity = EntityPool.Spawn(this.GetType());
            foreach(Type t in m_Components.Keys) {
                Component target = entity.AddComponent(t);
                target.Clone(m_Components[t]);
            }

            entity.isActive = this.isActive;

            return entity;
        }

        public override string ToString()
        {
            const string separator = ", ";
            StringBuilder sb = new StringBuilder();
            sb.Append($"Entity_{m_ID}(");
            Component[] components = m_Components.Values.ToArray();
            var lastSeparator = components.Length - 1;
            for (int i = 0; i < components.Length; i++) {
                sb.Append($"{components[i].ToString()}{(i < lastSeparator ? separator : "")}");
            }
            sb.Append(")");

            return sb.ToString();
        }

        public override int GetHashCode()
        {
            return GetInstanceID(); ;
        }

        public override bool Equals(object obj)
        {
            IEntity entity = obj as IEntity;
            return entity != null && this.GetHashCode() == entity.GetHashCode();
        }
    }
}
