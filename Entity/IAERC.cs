﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Creator.Entities
{
    public interface IAERC
    {
        int retainCount { get; }
        void Retain(object owner);
        void Release(object owner);
    }
}
