﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Creator.Entities
{
    class EntityIsNotRetainedByOwnerException : Exception
    {
        public EntityIsNotRetainedByOwnerException(IEntity entity, object owner)
            : base("Entity is not retained by this object!") {
        }
    }
}
