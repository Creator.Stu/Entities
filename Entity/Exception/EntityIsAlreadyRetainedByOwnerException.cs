﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Creator.Entities
{
    public class EntityIsAlreadyRetainedByOwnerException : Exception
    {
        public EntityIsAlreadyRetainedByOwnerException(IEntity entity, object owner) : base("Entity is already retained by this object!") { }
    }
}
