﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Creator.Entities
{
    public static class EntityPool
    {
        public readonly static Type IEntityType = typeof(IEntity);

        private readonly static Dictionary<Type, Stack<IEntity>> m_Pool = new Dictionary<Type, Stack<IEntity>>();

        private readonly static List<IEntity> _running = new List<IEntity>();

        public static ReadOnlyCollection<IEntity> Running => _running.AsReadOnly();

        private static int index = 0;

        static EntityPool() { }

        public static T Spawn<T>() where T : class, IEntity
        {
            return (T)Spawn(typeof(T));
        }

        public static IEntity Spawn(Type t)
        {
            if (!IEntityType.IsAssignableFrom(t)) {
                throw new InvalidCastException($"{t.FullName} is not a subclass of {IEntityType.FullName} !");
            }

            IEntity entity = null;
            if (m_Pool.ContainsKey(t) && m_Pool[t].Count > 0)
            {
                entity = m_Pool[t].Pop();
            }
            else {
                entity = (IEntity)Activator.CreateInstance(t);
            }
            entity.Initialize(++index);
            _running.Add(entity);
            return entity;
        }

        public static void Despawn(IEntity entity) {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            _running.Remove(entity);
            Type t = entity.GetType();
            if (!m_Pool.ContainsKey(t)) {
                m_Pool.Add(t, new Stack<IEntity>());
            }
            m_Pool[t].Push(entity);
        }
    }
}
