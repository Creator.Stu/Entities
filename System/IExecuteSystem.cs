﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Creator.Entities
{
    public interface IExecuteSystem : ISystem
    {
        void Execute();
    }
}
